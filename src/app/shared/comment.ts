import { Dish } from './dish';
export class Comment {
    rating: number;
    comment: string;
    author: string;
    date: string;
}