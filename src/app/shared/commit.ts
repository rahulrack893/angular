export class Commit {
    author: string;
    rating: number;
    comment: string;
    date: string;
}