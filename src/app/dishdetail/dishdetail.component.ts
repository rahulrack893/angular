import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { Comment } from './../shared/comment';
import { Params, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Commit } from './../shared/commit';
import { Location } from '@angular/common';
import { Dish } from './../shared/dish';
import { DishService } from '../services/dish.service';
import { switchMap } from 'rxjs/operators';
import { visibility, flyInOut, expand } from '../animations/app.animation';



@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block;'
  },
  animations: [
    visibility(),
    flyInOut(),
    expand()
  ]
})
export class DishdetailComponent implements OnInit {
  
  dish: Dish;
  dishcopy: Dish;
  errMess: string;
  dishIds: string[];
  prev: string;
  next: string;
  commitForm: FormGroup;
  commit: Commit;
  visibility = 'shown';
  @ViewChild('cform') commitFormDirective;
  formErrors = {
    'author': '',
    'rating': '',
    'comment': ''
  };
  validationMessages = {
    'author': {
      'required': 'Author name is required.',
      'minlength': 'Author name must be atleast 2 characters long',
      'maxlength': 'Author name cannot be more then 25 characterslong'
    },
    'rating': {
      'required': 'Rating is required.'
    },
    'comment': {
      'required': 'Comment is required.'
    }
  };
  
  

  constructor(private dishservice: DishService,
     private route: ActivatedRoute, 
     private location: Location,
     private cf: FormBuilder,
     @Inject('BaseURL') private BaseURL ) { 
    this.createForm();
  }

  ngOnInit() {
    this.dishservice.getDishIds()
    .subscribe((dishIds) => this.dishIds = dishIds);
    this.route.params.pipe(switchMap((params: Params) => {this.visibility = 'hidden'; return this.dishservice.getDish(params['id']);}))
  .subscribe((dish) => { this.dish = dish; this.dishcopy = dish; this.setPrevNext(dish.id); this.visibility = 'shown'; },
  errmess => this.errMess = <any>errmess);
}
  setPrevNext(dishId: string) {
    const index = this.dishIds.indexOf(dishId);
    this.prev = this.dishIds[(this.dishIds.length + index - 1) % this.dishIds.length];
    this.next = this.dishIds[(this.dishIds.length + index + 1) % this.dishIds.length];
  }
  goBack(): void {
    this.location.back();
  }
  createForm() {
    this.commitForm = this.cf.group({
      author: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
      rating: [5, [Validators.required]],
      comment: ['', [Validators.required]]

    });
    this.commitForm.valueChanges.subscribe(data => this.onValueChanged(data));
    this.onValueChanged(); //(re)set form validation messages.
  }
  onValueChanged(data?: any) {
    if(!this.commitForm) { return; }
    const form = this.commitForm;
    for(const field in this.formErrors) {
      if(this.formErrors.hasOwnProperty(field)) {
        //clear privious error message(if any)
        this.formErrors[field] = '';
        const control = form.get(field);
        if(control && control.dirty && !control.valid) {
          const messages = this.validationMessages[field];
          for(const key in control.errors) {
            if(control.errors.hasOwnProperty(key)) {
              this.formErrors[field] += messages[key] + '';
            }
          }
        }
      }
    }
  }
  onSubmit() {
    this.commit = this.commitForm.value;
    this.commit.date = new Date().toISOString();
    console.log(this.commit);
    this.dishcopy.comments.push(this.commit);
    this.dishservice.putDish(this.dishcopy)
    .subscribe(dish => {
      this.dish = dish; this.dishcopy = dish;
    },
    errmess => {
      this.dish = null;
      this.dishcopy = null; 
      this.errMess = <any>errmess;
    });
    this.commitFormDirective.resetForm();
    this.commitForm.reset({
      author: '',
      rating: 5,
      comment: ''
    });
    
  /*this.commit = this.commitForm.value;
    let nowDate = new Date();
    let newComment = {
      author: this.commit.author,
      rating: this.commit.rating,
      comment: this.commit.comment,
      date: nowDate.toISOString()
    }
    this.dish.comments.push(newComment);
    this.commitFormDirective.resetForm();
    this.commitForm.reset({
     author: '',
     rating: 5,
     comment: '' 
    });
    this.commitFormDirective.resetForm();*/
  }
 

}
